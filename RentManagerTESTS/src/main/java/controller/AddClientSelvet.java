package controller;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;

@WebServlet("/users/create")
public class AddClientSelvet extends HttpServlet {
    ClientService clientservice = ClientService.getInstance();
    VehicleService vehicleservice = VehicleService.getInstance();
    ReservationService reservationservice = ReservationService.getInstance();

    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/users/create.jsp");
        dispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String last_name = request.getParameter("last_name");
        System.out.println(last_name);
        String first_name = request.getParameter("first_name");
        System.out.println(first_name);
        String email = request.getParameter("email");
        System.out.println(email);
        Date naissance = Date.valueOf(request.getParameter("naissance"));
        System.out.println(naissance);

        Client newClient = new Client();
        newClient.setEmail(email);
        newClient.setNom(last_name);
        newClient.setPrenom(first_name);
        newClient.setNaissance(naissance);
        System.out.println(newClient);

        RequestDispatcher dispatcher;

        try {
            clientservice.create(newClient);

            request.setAttribute("nbUtilisateurs", clientservice.findAll().size());
            request.setAttribute("nbVehicle", vehicleservice.findAll().size());
            request.setAttribute("nbRents", reservationservice.FindAll().size());

            dispatcher = request.getRequestDispatcher("/WEB-INF/views/home.jsp");
        } catch (ServiceException e) {

            dispatcher = request.getRequestDispatcher("/WEB-INF/views/users/create.jsp");
            HttpSession session = request.getSession(true);
            session.setAttribute("erroraddclient",
                    "Il y a eu une erreur ! verifiez que l'utilisateur à plus de 18ans, que le mail n'est pa deja utilisé et que le nom et le prenom font plus de 3 caracteres. ");
            System.out.println(e);

        }
        dispatcher.forward(request, response);
    }

}
