package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Vehicle;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;

@WebServlet("/cars/details")
public class VehicleDetailsServlet extends HttpServlet {

    ClientService clientservice = ClientService.getInstance();
    VehicleService vehicleservice = VehicleService.getInstance();
    ReservationService reservationservice = ReservationService.getInstance();

    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/vehicles/details.jsp");

        int id = Integer.parseInt(request.getParameter("id"));
        System.out.println("Id" + id);

        try {
            request.setAttribute("reservation", reservationservice.findResaByVehicleId(id));
            System.out.println(reservationservice.findResaByVehicleId(id));
            request.setAttribute("nbRents", reservationservice.findResaByVehicleId(id).size());

            List<Integer> idclientliste = reservationservice.findClientByVehicleId(id);
            System.out.println(idclientliste);
            List<Client> clientList = new ArrayList<Client>();
            System.out.println(clientList);

            for (Integer idclient : idclientliste) {
                Client client = clientservice.findById(idclient);
                clientList.add(client);
            }
            System.out.println(clientList);
            request.setAttribute("client", clientList);

        } catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        dispatcher.forward(request, response);

    }

}
