package controller;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;

@WebServlet("/rents/modif")
public class RentsModifServlet extends HttpServlet {

    ClientService clientservice = ClientService.getInstance();
    VehicleService vehicleservice = VehicleService.getInstance();
    ReservationService reservationservice = ReservationService.getInstance();

    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        System.out.println("Id " + id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/rents/modif.jsp");
        request.setAttribute("id", id);

        try {
            request.setAttribute("users", clientservice.findAll());
            request.setAttribute("vehicles", vehicleservice.findAll());
            request.setAttribute("resaselected", reservationservice.findResaById(id));

        } catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        dispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(true);

        int id = Integer.parseInt(request.getParameter("id"));
        System.out.println("Id " + id);

        String vehicle = request.getParameter("car");
        String client = request.getParameter("client");
        String begin = request.getParameter("begin");
        String end = request.getParameter("end");

        try {
            if (vehicle.length() > 0) {
                System.out.println(vehicle);
                int vehicleid = Integer.parseInt(vehicle);
                reservationservice.updateVehicle(vehicleid, id);
            } else {
            }
            if (client.length() > 0) {
                System.out.println(client);
                int clientid = Integer.parseInt(client);
                reservationservice.updateClient(clientid, id);
            } else {
            }
            if (begin.length() > 0) {
                System.out.println(begin);
                reservationservice.updateDebut(begin, id);
            } else {
            }
            if (end.length() > 0) {
                System.out.println(end);
                reservationservice.updateFin(end, id);
            } else {
            }

        } catch (ServiceException e) {
            session.setAttribute("errorrents", e.getMessage());

            response.sendRedirect("/RentManager/rents/modif?id=" + id);
            return;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        response.sendRedirect("/RentManager/rents");
        return;

    }
}
