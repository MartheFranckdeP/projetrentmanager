package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;

@WebServlet("/cars/modif")
public class VehicleModifServlet extends HttpServlet {

    ClientService clientservice = ClientService.getInstance();
    VehicleService vehicleservice = VehicleService.getInstance();
    ReservationService reservationservice = ReservationService.getInstance();

    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        System.out.println("Id " + id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/vehicles/modif.jsp");
        request.setAttribute("id", id);
        try {
            request.setAttribute("vehicleselected", vehicleservice.findById(id));
        } catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        dispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(true);

        int id = Integer.parseInt(request.getParameter("id"));
        System.out.println("Id " + id);

        String manufacturer = request.getParameter("manufacturer");
        String modele = request.getParameter("modele");
        String seats = request.getParameter("seats");

        try {
            if (manufacturer.length() > 0) {
                System.out.println(manufacturer);

                vehicleservice.updateConstructeur(manufacturer, id);

            } else {
            }
            if (modele.length() > 0) {
                System.out.println(modele);

                vehicleservice.updateModele(modele, id);

            } else {
            }
            if (seats.length() > 0) {
                System.out.println(seats);
                int nbseat = Integer.parseInt(seats);

                vehicleservice.updateNb_places(nbseat, id);

            } else {
            }
        } catch (ServiceException e) {
            session.setAttribute("errorvehicle",
                    "La voiture doit avoir un constructeur, un modèle et doit avoir entre 2 et 9 places ! ");
            e.printStackTrace();
            response.sendRedirect("/RentManager/cars/modif?id=" + id);
            return;
        }

        response.sendRedirect("/RentManager/cars");
        return;

    }

}
