package controller;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Reservation;
import com.ensta.rentmanager.model.Vehicle;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;

@WebServlet("/rents/create")
public class RentsAddServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    ReservationService reservationservice = ReservationService.getInstance();
    ClientService clientservice = ClientService.getInstance();
    VehicleService vehicleService = VehicleService.getInstance();

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/rents/create.jsp");

        try {
            request.setAttribute("users", clientservice.findAll());
            request.setAttribute("vehicles", vehicleService.findAll());

        } catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        dispatcher.forward(request, response);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(true);

        int vehicle = Integer.parseInt(request.getParameter("car"));
        System.out.println(vehicle);
        int client = Integer.parseInt(request.getParameter("client"));
        System.out.println(client);
        Date begin = Date.valueOf(request.getParameter("begin"));
        System.out.println(begin);
        Date end = Date.valueOf(request.getParameter("end"));
        System.out.println(end);

        Reservation newRents = new Reservation();
        newRents.setClient_id(client);
        newRents.setVehicle_id(vehicle);
        newRents.setDebut(begin);
        newRents.setFin(end);
        System.out.println(newRents);

        try {

            reservationservice.create(newRents);
            response.sendRedirect("/RentManager/rents");
            return;
        } catch (ServiceException e) {

            session.setAttribute("errorrents", e.getMessage());

            response.sendRedirect("/RentManager/rents/create");
            return;
        }

    }
}
