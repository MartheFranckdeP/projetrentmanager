package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;
import com.ensta.rentmanager.model.*;

@WebServlet("/users/details")
public class UserDetailsServlet extends HttpServlet {

    ClientService clientservice = ClientService.getInstance();
    VehicleService vehicleservice = VehicleService.getInstance();
    ReservationService reservationservice = ReservationService.getInstance();

    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/users/details.jsp");

        int id = Integer.parseInt(request.getParameter("id"));
        System.out.println("Id " + id);

        try {

            Client newclient = clientservice.findById(id);

            request.setAttribute("idutilisateur", newclient.getId());
            request.setAttribute("prenom", newclient.getPrenom());
            request.setAttribute("nomAttribute", newclient.getNom());
            request.setAttribute("nbRents", reservationservice.findClientReservations(id).size());

            request.setAttribute("reservation", reservationservice.findClientReservations(id));

            List<Integer> idvehiculeliste = reservationservice.findVehicleByClientId(id);
            System.out.println(idvehiculeliste);
            List<Vehicle> vehicleList = new ArrayList<Vehicle>();
            System.out.println(vehicleList);

            for (Integer idvehicule : idvehiculeliste) {
                Vehicle vehicle = vehicleservice.findById(idvehicule);
                vehicleList.add(vehicle);
            }
            System.out.println(vehicleList);
            request.setAttribute("vehicule", vehicleList);

        } catch (ServiceException e) {
            request.setAttribute("nomUtilisateur", "Y a une erreur !");
            request.setAttribute("prenomAttribute", "Y a une erreur !");
        }

        dispatcher.forward(request, response);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}
