package controller;

import java.io.IOException;
import java.sql.Date;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;

@WebServlet("/users/modif")
public class UserModifServlet extends HttpServlet {
    ClientService clientservice = ClientService.getInstance();
    VehicleService vehicleservice = VehicleService.getInstance();
    ReservationService reservationservice = ReservationService.getInstance();

    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        System.out.println("Id " + id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/users/modif.jsp");
        request.setAttribute("id", id);
        try {
            request.setAttribute("userselected", clientservice.findById(id));
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        dispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int id = Integer.parseInt(request.getParameter("id"));
        System.out.println("Id " + id);

        HttpSession session = request.getSession(true);

        String last_name = request.getParameter("last_name");
        String first_name = request.getParameter("first_name");
        String email = request.getParameter("email");
        String naissance = request.getParameter("naissance");

        try {
            if (last_name.length() > 0) {
                session.setAttribute("erroranomsclient", "Le nom est le prénom doivent faire plus de 3 caractere ! ");
                System.out.println(last_name);
                clientservice.updateName(last_name, id);

            } else {
            }
            if (first_name.length() > 0) {
                session.setAttribute("erroranomsclient", "Le nom est le prénom doivent faire plus de 3 caractere ! ");
                System.out.println(first_name);
                clientservice.updatePrenom(first_name, id);

            } else {
            }
            if (email.length() > 0) {
                System.out.println(email);
                session.setAttribute("erroraddmailclient", "Cette adresse mail est déjà utilisée ! ");
                clientservice.updateEmail(email, id);

            } else {
            }

            if (naissance.length() > 0) {
                session.setAttribute("erroraddclient",
                        "On ne peut pas rejeunir un client à ce point là, il doit avoir plus de 18 ans ! ");
                System.out.println(naissance);
                Client client;
                client = clientservice.findById(id);
                System.out.println("Client " + client);
                System.out.println("id " + id);
                System.out.println("naissance " + naissance);
                clientservice.updateBirth(naissance, id, client);
            } else {
            }

            response.sendRedirect("/RentManager/users");
            return;

        } catch (ServiceException e) {
            System.out.println(e);
            response.sendRedirect("/RentManager/users/modif?id=" + id);
            return;
        }

    }
}
