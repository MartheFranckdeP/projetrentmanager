package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ensta.rentmanager.exception.ServiceException;

import com.ensta.rentmanager.model.Vehicle;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;

@WebServlet("/cars/create")
public class VehicleAddServlet extends HttpServlet {
    ClientService clientservice = ClientService.getInstance();
    VehicleService vehicleservice = VehicleService.getInstance();
    ReservationService reservationservice = ReservationService.getInstance();

    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/vehicles/create.jsp");
        dispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(true);

        String manufacturer = request.getParameter("manufacturer");
        System.out.println(manufacturer);
        String modele = request.getParameter("modele");
        System.out.println(modele);
        int seats = Integer.parseInt(request.getParameter("seats"));
        System.out.println(seats);

        Vehicle newVehicle = new Vehicle();
        newVehicle.setConstructeur(manufacturer);
        newVehicle.setModele(modele);
        newVehicle.setNb_place(seats);
        System.out.println(newVehicle);

        try {

            vehicleservice.create(newVehicle);

        } catch (ServiceException e) {
            session.setAttribute("errorvehicle",
                    "La voiture doit avoir un constructeur, un modèle et doit avoir entre 2 et 9 places ! ");
            e.printStackTrace();
            response.sendRedirect("/RentManager/cars/create");
            return;
        }
        response.sendRedirect("/RentManager/cars");
        return;
    }
}
