package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Reservation;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;

@WebServlet("/rents/delete")
public class RentsDeleteServlet extends HttpServlet {

    ReservationService reservationservice = ReservationService.getInstance();

    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/rents/delete.jsp");

        int id = Integer.parseInt(request.getParameter("id"));
        System.out.println("Id " + id);

        try {

            reservationservice.deleteReservation(id);

        } catch (ServiceException e) {
            request.setAttribute("nomUtilisateur", "Y a une erreur !");
            request.setAttribute("prenomAttribute", "Y a une erreur !");
        }

        response.sendRedirect("/RentManager/rents");
        return;

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}
