package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Reservation;
import com.ensta.rentmanager.model.Vehicle;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;

@WebServlet("/cars/delete")
public class VehicleDeleteServlet extends HttpServlet {

	
	VehicleService vehicleservice = VehicleService.getInstance();
	ReservationService reservationservice = ReservationService.getInstance();
	
	private static final long serialVersionUID=1L;
	
	protected void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/users/delete.jsp");
		
	int id = Integer.parseInt(request.getParameter("id"));
	System.out.println("Id "+id);
	

	
	try {
		
		List<Reservation> ListResa = new ArrayList<Reservation>();
		ListResa.addAll(reservationservice.findResaByVehicleId(id));
		System.out.print(ListResa);
		for (Reservation resa : ListResa) {
			int resaid = resa.getId();
			reservationservice.deleteReservation(resaid);
		}

	vehicleservice.delete(id);

	}catch(ServiceException e) {
		System.out.println("y a un probleme a la suppression du vehicule");
	} catch (DaoException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	response.sendRedirect("/RentManager/cars");
	return;
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		doGet(request, response);
	}
	
}
