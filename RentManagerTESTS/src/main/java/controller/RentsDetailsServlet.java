package controller;

import java.io.IOException;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Reservation;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;

@WebServlet("/rents/details")
public class RentsDetailsServlet extends HttpServlet {
    ClientService clientservice = ClientService.getInstance();
    VehicleService vehicleservice = VehicleService.getInstance();
    ReservationService reservationservice = ReservationService.getInstance();

    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/rents/details.jsp");
        int id = Integer.parseInt(request.getParameter("id"));

        System.out.println("Id " + id);

        try {
            int idvehicle = reservationservice.findVehicleIdByRentId(id);
            System.out.println(idvehicle);
            request.setAttribute("vehicule", vehicleservice.findById(idvehicle));
            request.setAttribute("vehiculemodele", (vehicleservice.findById(idvehicle)).getModele());
            request.setAttribute("vehiculeconstructeur", (vehicleservice.findById(idvehicle)).getConstructeur());
            System.out.println(vehicleservice.findById(idvehicle));

            int idclient = reservationservice.findClientIdByRentId(id);
            System.out.println(idclient);
            request.setAttribute("users", clientservice.findById(idclient));
            request.setAttribute("userslastname", (clientservice.findById(idclient)).getNom());
            request.setAttribute("usersfirstname", (clientservice.findById(idclient)).getPrenom());
            System.out.println(clientservice.findById(idclient));

        } catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        dispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);

    }
}
