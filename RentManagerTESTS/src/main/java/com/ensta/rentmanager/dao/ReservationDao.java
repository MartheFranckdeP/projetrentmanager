package com.ensta.rentmanager.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.jstl.sql.Result;

import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.model.Reservation;
import com.ensta.rentmanager.model.Vehicle;
import com.ensta.rentmanager.persistence.ConnectionManager;

public class ReservationDao {

    private static ReservationDao instance = null;
    private static ReservationDao instanceTest = null;

    private ReservationDao() {
    }

    private boolean test;

    private ReservationDao(boolean test) {
        this.test = test;
    }

    public static ReservationDao getInstance(boolean test) {
        if (test) {
            if (instanceTest == null) {
                instanceTest = new ReservationDao(true);
                return instanceTest;
            }
        } else {
            if (instance == null) {
                instance = new ReservationDao(false);
            }
        }
        return instance;
    }

    private static final String CREATE_RESERVATION_QUERY = "INSERT INTO Reservation(client_id, vehicle_id, debut, fin) VALUES(?, ?, ?, ?);";
    private static final String DELETE_RESERVATION_QUERY = "DELETE FROM Reservation WHERE id=?;";
    private static final String FIND_RESERVATIONS_BY_CLIENT_QUERY = "SELECT id ,vehicle_id, debut, fin FROM Reservation WHERE client_id=?;";
    private static final String FIND_RESERVATIONS_BY_VEHICLE_QUERY = "SELECT id, client_id,debut, fin FROM Reservation WHERE vehicle_id=?;";
    private static final String FIND_RESERVATIONS_QUERY = "SELECT id, client_id, vehicle_id, debut, fin FROM Reservation;";
    private static final String FIND_VEHICLES_ID_BY_CLIENT_ID = "SELECT  vehicle_id FROM Reservation WHERE client_id=?;";
    private static final String FIND_CLIENT_ID_BY_VEHICLES_ID = "SELECT  client_id FROM Reservation WHERE vehicle_id=?;";
    private static final String GET_VEHICLE_ID_WITH_RESERVATION_ID = "SELECT vehicle_id FROM Reservation WHERE id=?;";
    private static final String GET_CLIENT_ID_WITH_RESERVATION_ID = "SELECT client_id FROM Reservation WHERE id=?;";

    private static final String FIND_RESERVATION_QUERY = "SELECT client_id, vehicle_id, debut, fin FROM Reservation WHERE id=?;";

    private static final String UPDATE_RESERVATION_BEGIN = "UPDATE Reservation SET debut=? WHERE id=?;";
    private static final String UPDATE_RESERVATION_FIN = "UPDATE Reservation SET fin=? WHERE id=?;";
    private static final String UPDATE_RESERVATION_CLIENT = "UPDATE Reservation SET client_id=? WHERE id=?;";
    private static final String UPDATE_RESERVATION_VEHICLE = "UPDATE Reservation SET vehicle_id=? WHERE id=?;";

    public Reservation findResaById(int ResaId) throws DaoException {
        Reservation resa = new Reservation();
        try (Connection conn = test ? ConnectionManager.getConnectionForTest() : ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(FIND_RESERVATION_QUERY);) {

            statement.setInt(1, ResaId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                resa.setId(ResaId);
                resa.setClient_id(resultSet.getInt(1));
                resa.setVehicle_id(resultSet.getInt(2));
                resa.setDebut(resultSet.getDate(3));
                resa.setFin(resultSet.getDate(4));

            }
            return resa;

        } catch (SQLException e) {
            throw new DaoException("Erreur lors de l'affichage des véhicules :" + e.getMessage());
        }
    }

    public long updateDebut(String val, int id) throws DaoException {
        try (Connection conn = test ? ConnectionManager.getConnectionForTest() : ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(UPDATE_RESERVATION_BEGIN);) {
            statement.setString(1, val);
            statement.setInt(2, id);

            long result = statement.executeUpdate();
            return result;

        } catch (SQLException e) {
            throw new DaoException("Erreur lors de l'update :" + e.getMessage());
        }
    }

    public long updateFin(String val, int id) throws DaoException {
        try (Connection conn = test ? ConnectionManager.getConnectionForTest() : ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(UPDATE_RESERVATION_FIN);) {
            statement.setString(1, val);
            statement.setInt(2, id);

            long result = statement.executeUpdate();
            return result;

        } catch (SQLException e) {
            throw new DaoException("Erreur lors de l'update :" + e.getMessage());
        }
    }

    public long updateVehicle(int val, int id) throws DaoException {
        try (Connection conn = test ? ConnectionManager.getConnectionForTest() : ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(UPDATE_RESERVATION_VEHICLE);) {
            statement.setInt(1, val);
            statement.setInt(2, id);

            long result = statement.executeUpdate();
            return result;

        } catch (SQLException e) {
            throw new DaoException("Erreur lors de l'update :" + e.getMessage());
        }
    }

    public long updateClient(int val, int id) throws DaoException {
        try (Connection conn = test ? ConnectionManager.getConnectionForTest() : ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(UPDATE_RESERVATION_FIN);) {
            statement.setInt(1, val);
            statement.setInt(2, id);

            long result = statement.executeUpdate();
            return result;

        } catch (SQLException e) {
            throw new DaoException("Erreur lors de l'update :" + e.getMessage());
        }
    }

    public long create(Reservation reservation) throws DaoException {
        try (Connection conn = test ? ConnectionManager.getConnectionForTest() : ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(CREATE_RESERVATION_QUERY);) {
            statement.setInt(1, reservation.getClient_id());
            statement.setInt(2, reservation.getVehicle_id());
            statement.setDate(3, reservation.getDebut());
            statement.setDate(4, reservation.getFin());

            long result = statement.executeUpdate();
            return result;

        } catch (SQLException e) {
            throw new DaoException("Erreur lors de la création :" + e.getMessage());
        }
    }

    public long delete(int id) throws DaoException {
        try (Connection conn = test ? ConnectionManager.getConnectionForTest() : ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(DELETE_RESERVATION_QUERY);) {
            statement.setInt(1, id);

            long result = statement.executeUpdate();
            return result;

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            throw new DaoException("Erreur lors de la suppression :" + e.getMessage());
        }

    }

    public List<Reservation> findResaByClientId(int clientId) throws DaoException {
        List<Reservation> resultList = new ArrayList<Reservation>();
        try (Connection conn = test ? ConnectionManager.getConnectionForTest() : ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(FIND_RESERVATIONS_BY_CLIENT_QUERY);) {

            statement.setInt(1, clientId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Reservation reservation = new Reservation(resultSet.getInt(1), clientId, resultSet.getInt(2),
                        resultSet.getDate(3), resultSet.getDate(4));
                resultList.add(reservation);

            }
            return resultList;

        } catch (SQLException e) {
            throw new DaoException("Erreur lors de l'affichage des véhicules :" + e.getMessage());
        }
    }

    public List<Reservation> findResaByVehicleId(int vehicleId) throws DaoException {
        List<Reservation> resultList = new ArrayList<Reservation>();
        try (Connection conn = test ? ConnectionManager.getConnectionForTest() : ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(FIND_RESERVATIONS_BY_VEHICLE_QUERY);) {
            statement.setInt(1, vehicleId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Reservation reservation = new Reservation(resultSet.getInt(1), resultSet.getInt(2), vehicleId,
                        resultSet.getDate(3), resultSet.getDate(4));
                resultList.add(reservation);
                System.out.println("dao reservation selon vehicule id : " + reservation);

            }
            return resultList;

        } catch (SQLException e) {
            throw new DaoException("Erreur lors de l'affichage des véhicules :" + e.getMessage());
        }
    }

    public List<Reservation> findAll() throws DaoException {
        List<Reservation> resultList = new ArrayList<Reservation>();
        try (Connection conn = test ? ConnectionManager.getConnectionForTest() : ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(FIND_RESERVATIONS_QUERY);) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Reservation reservation = new Reservation(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3),
                        resultSet.getDate(4), resultSet.getDate(5));
                resultList.add(reservation);

            }
            return resultList;

        } catch (SQLException e) {
            throw new DaoException("Erreur lors de l'affichage des véhicules :" + e.getMessage());
        }
    }

    public List<Integer> findVehicleByClientId(int clientId) throws DaoException {
        List<Integer> resultList = new ArrayList<Integer>();
        try (Connection conn = test ? ConnectionManager.getConnectionForTest() : ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(FIND_VEHICLES_ID_BY_CLIENT_ID);) {
            statement.setInt(1, clientId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int vehicleid = new Integer(resultSet.getInt(1));

                resultList.add(vehicleid);

            }
            return resultList;

        } catch (SQLException e) {
            throw new DaoException("Erreur lors de l'affichage des véhicules :" + e.getMessage());
        }
    }

    public List<Integer> findClientByVehicleId(int vehicleId) throws DaoException {
        List<Integer> resultList = new ArrayList<Integer>();
        try (Connection conn = test ? ConnectionManager.getConnectionForTest() : ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(FIND_CLIENT_ID_BY_VEHICLES_ID);) {
            statement.setInt(1, vehicleId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int clientId = new Integer(resultSet.getInt(1));
                resultList.add(clientId);
            }
            return resultList;

        } catch (SQLException e) {
            throw new DaoException("Erreur lors de l'affichage des véhicules :" + e.getMessage());
        }
    }

    public int findVehicleIdByRentId(int rentid) throws DaoException {
        try (Connection conn = test ? ConnectionManager.getConnectionForTest() : ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(GET_VEHICLE_ID_WITH_RESERVATION_ID);) {
            statement.setInt(1, rentid);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            Integer vehicleid = new Integer(resultSet.getInt(1));

            return vehicleid;

        } catch (SQLException e) {
            throw new DaoException("Erreur lors de l'affichage des véhicules :" + e.getMessage());
        }
    }

    public int findClientIdByRentId(int rentid) throws DaoException {
        try (Connection conn = test ? ConnectionManager.getConnectionForTest() : ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(GET_CLIENT_ID_WITH_RESERVATION_ID);) {
            statement.setInt(1, rentid);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            Integer vehicleid = new Integer(resultSet.getInt(1));

            return vehicleid;

        } catch (SQLException e) {
            throw new DaoException("Erreur lors de l'affichage des véhicules :" + e.getMessage());
        }
    }
}
