package com.ensta.rentmanager.service;

import java.sql.Date;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.util.List;

import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Reservation;
import com.ensta.rentmanager.dao.ClientDao;
import com.ensta.rentmanager.dao.ReservationDao;
import com.ensta.rentmanager.service.*;

public class ClientService {

    private static ClientService instance = null;

    private ClientService() {
    }

    public static ClientService getInstance() {
        if (instance == null) {
            instance = new ClientService();
        }

        return instance;
    }

    // use test base or not
    ClientDao clientdao = ClientDao.getInstance(false);
    ValidatorService validate = ValidatorService.getInstance();
    ReservationDao reservationdao = ReservationDao.getInstance(false);

    public List<Client> FindAll() throws ServiceException {
        try {
            return clientdao.findAll();

        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public long create(Client client) throws ServiceException {

        validate.checkAge(client.getNaissance().toLocalDate());
        String newemail = client.getEmail();
        String nom = client.getNom();
        String prenom = client.getPrenom();

        try {
            validate.checkLengh3(nom);
            validate.checkLengh3(prenom);
            validate.checkEmail(newemail);
            return clientdao.create(client);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }

    }

    public Client findById(int id) throws ServiceException {
        try {
            return clientdao.show_client(id);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }

    }

    public long updateName(String new_name, int id) throws ServiceException {
        try {
            validate.checkLengh3(new_name);
            return clientdao.updateName(new_name, id);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public long deleteClient(int id) throws ServiceException {
        try {
            validate.DeleteAllResaAssciatetoTheClient(id);
            return clientdao.delete(id);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public long updatePrenom(String new_prenom, int id) throws ServiceException {
        try {
            return clientdao.updatePrenom(new_prenom, id);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public long updateBirth(String new_date, int id, Client client) throws ServiceException {

        validate.checkAge(client.getNaissance().toLocalDate());
        try {
            return clientdao.updateBirth(new_date, id);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public long updateEmail(String new_email, int id) throws ServiceException {

        try {
            validate.checkEmail(new_email);
            return clientdao.updateMail(new_email, id);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public void printBetween() {
        System.out.println("1 - client");
        System.out.println("2 - véhicule");
        System.out.println("3 - réservations");
        System.out.println("4 - quitter le programme");
    }

    public List<Client> findAll() throws ServiceException {
        try {
            return clientdao.findAll();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }

    }
    
    public void printChoix() {
        System.out.println("1 : Ajouter un client");
        System.out.println("2 : Supprimer un client");
        System.out.println("3 : Lister les clients");
        System.out.println("4 : Modifier les données d'un client");
        System.out.println("5 : Quitter le programme");
    }

    public void printUpdate(int id) {
        System.out.println("-----Client n°" + id);
        System.out.println("1 : Modifier le nom du client");
        System.out.println("2 : Modifier le prénom du client");
        System.out.println("3 : Modifier l'email du client");
        System.out.println("4 : Modifier la date de naissance du client");
        System.out.println("5 : revenir au menu principal");
        System.out.print("Entrer le numéro ");

    }

}
