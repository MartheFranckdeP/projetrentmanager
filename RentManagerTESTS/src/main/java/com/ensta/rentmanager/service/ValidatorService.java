package com.ensta.rentmanager.service;

//import java.sql.Date;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Reservation;
import com.ensta.rentmanager.model.Vehicle;
import com.ensta.rentmanager.dao.ClientDao;
import com.ensta.rentmanager.dao.ReservationDao;

public class ValidatorService {

    private static ValidatorService instance = null;

    private ValidatorService() {
    }

    public static ValidatorService getInstance() {
        if (instance == null) {
            instance = new ValidatorService();
        }

        return instance;
    }

    ClientDao clientdao = ClientDao.getInstance(false);
    ReservationDao reservationdao = ReservationDao.getInstance(false);

    void checkAge(Temporal annee) throws ServiceException {

        // calcule de lage
        long age = ChronoUnit.YEARS.between(annee, LocalDate.now());
        System.out.println("age " + age);
        // verification de lage
        if (age < 18) {
            throw new ServiceException("le client doit avoir 18 ans");
        }
    }

    void checkEmail(String newemail) throws ServiceException, DaoException {
        // Recuperation de tous les client
        List<Client> clientlist = clientdao.findAll();
        System.out.print("client list" + clientlist);
        // verification de chaque @mail
        for (Client clientsmail : clientlist) {

            String mail = clientsmail.getEmail();
            System.out.println("mail dans la bdd" + mail);
            System.out.println("nouveau mail " + newemail);

            if (mail.equals(newemail)) {
                throw new ServiceException("cette adresse mail est déjà utilisée !");

            }

        }
    }

    void checkLengh3(String val) throws ServiceException, DaoException {

        if (val.length() < 3) {
            throw new ServiceException("Le nom et le prenom doivent avoir plus de 3 caractères");
        }

    }

    void checkperiodelength(java.util.Date debut, java.util.Date fin) throws ServiceException {
        // calcul de la duree de la periode voulue
        long diff = fin.getTime() - debut.getTime();
        float length = (diff / (1000 * 60 * 60 * 24));
        System.out.println("duree " + length);
        // verification de la duree de la periode
        if (length > 7) {
            throw new ServiceException(
                    "La periode de location est trop longue, elle ne doit pas etre supérieur à 7 jours");
        }

    }

    public void checkperiodes(java.util.Date newdebut, java.util.Date newfin, int vehicleId, int id)
            throws ServiceException, DaoException {
        // recuperation de toutes reservaion du vehicule concerne
        List<Reservation> reservationsliste = reservationdao.findResaByVehicleId(vehicleId);
        System.out.println(reservationsliste);

        if (reservationsliste.isEmpty()) {
            System.out.println("Pas de reservations associées à cette voiture pour le moment");
        }

        for (Reservation resa : reservationsliste) {
            if (resa.getId() != id) {
                // recuperation des dates de reservation deja dans la base de donnees
                Date olddebut = resa.getDebut();
                Date oldfin = resa.getFin();
                System.out.println("olddebut " + olddebut);
                System.out.println("oldfin  " + oldfin);
                // comparaision des nouvelles dates et des anciennes
                if (oldfin.before(newdebut) || newfin.before(olddebut)) {
                    System.out.println(oldfin.before(newdebut));
                    System.out.println(newfin.before(olddebut));
                    System.out.println("Les dates de reservations peuvent etre appliquees");

                } else {
                    throw new ServiceException(
                            "La voiture ne peut peut pas etre louee 2 fois la meme periode, la duplication ne fait pas partie de ses options. Veuillez choisir d'autre dates de reservation !");

                }
            }
        }
    }

    public void CheckcarsNbPaces(int nb_place) throws ServiceException {

        if (nb_place < 2 || nb_place > 9) {
            throw new ServiceException("Le nombre de place de la voiture doit-etre compris entre 2 et 9 !");
        }

    }

    public void CheckcarsModeleConstructeur(String string) throws ServiceException {

        if (string.length() < 0) {
            throw new ServiceException("Le vehicule doit avoir un modele et un constructeur");
        }
    }

    public void DeleteAllResaAssciatetoTheVehicule(int id) throws ServiceException, DaoException {
        System.out.println("id du vehicule" + id);
        // recuperation de toutes les reservation liees à la voiture que l'on veut
        // supprimer
        List<Reservation> ListResa = new ArrayList<Reservation>();
        ListResa.addAll(reservationdao.findResaByVehicleId(id));
        System.out.print(ListResa);
        // Supprimer ces reservation
        for (Reservation resa : ListResa) {
            int resaid = resa.getId();
            reservationdao.delete(resaid);
        }
    }

    public void DeleteAllResaAssciatetoTheClient(int id) throws ServiceException, DaoException {
        System.out.println("id du vehicule" + id);
        // recuperation de toutes les reservation liees au client que l'on veut
        // supprimer
        List<Reservation> ListResa = new ArrayList<Reservation>();
        ListResa.addAll(reservationdao.findResaByClientId(id));
        System.out.print(ListResa);
        // Supprimer ces reservation
        for (Reservation resa : ListResa) {
            int resaid = resa.getId();
            reservationdao.delete(resaid);
        }
    }

    public void CheckLongResaPeriod(java.util.Date newdebut, java.util.Date newfin, int idvehicle, int idresa,
            boolean addOrModif) throws ServiceException, DaoException {
        // recuperation de toutes les reservations liees a la voiture
        List<Reservation> reservationsliste = reservationdao.findResaByVehicleId(idvehicle);
        System.out.println(reservationsliste);

        if (reservationsliste.isEmpty()) {
            System.out.println("Pas de reservations associées à cette voiture pour le moment");
        } else {
            // si c'est un ajout de reservation qui est demande
            if (addOrModif == true) {
                // ona joute a la liste de reservarions une reseravtion avec les dates supposees
                // bonnes
                Reservation newresa = new Reservation();
                java.sql.Date sqldebut = new java.sql.Date(newdebut.getTime());
                java.sql.Date sqlfin = new java.sql.Date(newfin.getTime());
                newresa.setDebut(sqldebut);
                newresa.setFin(sqlfin);
                newresa.setVehicle_id(idvehicle);
                System.out.println("Reservation ajoutée à la liste pour add " + newresa);
                reservationsliste.add(newresa);
            } else {
                // Si on veux modifier les date d'une reservation existante
                int indexresa = 0;
                int lenghresa = reservationsliste.size();
                System.out.println(lenghresa);
                for (Reservation resa : reservationsliste) {
                    System.out.println(resa.getId() == idresa);
                    System.out.println(indexresa < lenghresa);
                    System.out.println(indexresa);
                    // On cherche la reseravtion qu'on veut modifier
                    if (resa.getId() == idresa && indexresa < lenghresa) {
                        System.out.println(
                                "Si modif, element de la liste à supprimé : " + reservationsliste.get(indexresa));
                        Reservation resatodelete = reservationsliste.get(indexresa);
                        // on supprime cette reservation pour que les anciennes dates ne soient plus
                        // prises en compte
                        reservationsliste.remove(resatodelete);
                        Reservation newresa = new Reservation();
                        java.sql.Date sqldebut = new java.sql.Date(newdebut.getTime());
                        java.sql.Date sqlfin = new java.sql.Date(newfin.getTime());
                        // On ajoute une reservation avec les bonnes dates
                        newresa.setDebut(sqldebut);
                        newresa.setFin(sqlfin);
                        newresa.setVehicle_id(idvehicle);
                        reservationsliste.add(newresa);
                        System.out.println("Reservation ajoutée à la liste pour les modif" + newresa);
                        break;
                    }
                    indexresa += 1;
                }
            }

            for (Reservation resa : reservationsliste) {
                // Récupération du nb de jour de la réservation "resa"
                int nbJourResa = (int) ((resa.getFin().getTime() - resa.getDebut().getTime()) / (1000 * 60 * 60 * 24)); // TODO
                // Recherche des réservations consécutives
                findConsecutifResa(resa, reservationsliste, nbJourResa);
            }
        }

    }

    private void findConsecutifResa(Reservation resa, List<Reservation> reservationsliste, int nbJourCumul)
            throws ServiceException {
        // Calcul du lendemain de la date de fin la réservation à tester
        Calendar calDateFinResa = Calendar.getInstance();
        calDateFinResa.setTime(resa.getFin());
        calDateFinResa.add(Calendar.DATE, 1);
        Date lendemainDateFinResa = (Date) calDateFinResa.getTime();
        System.out.println("lendemainDateFinResa " + lendemainDateFinResa);

        int idxResa = 0;
        Reservation resa1 = null;

        boolean consecutifFound = false;
        System.out.println(" ");
        System.out.println("taille de liste reservation " + reservationsliste.size());
        System.out.println("resa " + resa);

        // On bloque tant qu'on a pas listé toutes les réservation OU qu'on n'a pas
        // trouvé de réservation consécutive
        while ((idxResa < reservationsliste.size()) && (!consecutifFound)) {
            System.out.println(idxResa < reservationsliste.size());
            System.out.println(!consecutifFound);
            System.out.println(idxResa);

            resa1 = reservationsliste.get(idxResa);
            System.out.println("resa1" + resa1);
            System.out.println("resa.getId() != resa1.getId() " + (resa.getId() != resa1.getId()));
            if (resa.getId() != resa1.getId()) {

                System.out.println("resa1.getDebut().equals(lendemainDateFinResa) "
                        + resa1.getDebut().equals(lendemainDateFinResa));
                System.out.println("resa1.getDebut()) " + resa1.getDebut());
                // Si on trouve une réservation qui suit celle que l'on test
                if (resa1.getDebut().equals(lendemainDateFinResa)) {

                    // Récupération du nb de jour de la réservation "resa1"
                    int nbJourResa1 = (int) ((resa1.getFin().getTime() - resa1.getDebut().getTime())
                            / (1000 * 60 * 60 * 24)); // TODO

                    // Ajout de ce nombre de jours au cumul déjà connu
                    nbJourCumul += nbJourResa1;
                    System.out.println("nb de jour en cumul " + nbJourCumul);

                    // Si cumul supérieur à 30 jours, on remonte l'exception
                    if (nbJourCumul > 29) {
                        System.out.println(" ERREUR !!! ");
                        throw new ServiceException(" La voiture a été réservée plus de 30 jours d'affilée !");
                    } else {
                        // Sinon, on recherche la réservation consécutive suivante
                        consecutifFound = true;
                        System.out.println("nb de jour en cumul avant de relancer une boucle " + nbJourCumul);
                        findConsecutifResa(resa1, reservationsliste, nbJourCumul);
                    }
                }
            }
            idxResa++;
        }

    }

}
