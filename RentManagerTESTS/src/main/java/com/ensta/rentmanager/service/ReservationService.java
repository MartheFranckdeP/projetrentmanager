package com.ensta.rentmanager.service;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.temporal.Temporal;
import java.util.Date;
import java.util.List;

import com.ensta.rentmanager.dao.ReservationDao;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Reservation;

public class ReservationService {
    private static ReservationService instance = null;

    private ReservationService() {
    }

    public static ReservationService getInstance() {
        if (instance == null) {
            instance = new ReservationService();
        }

        return instance;
    }

    // use test base or not
    ReservationDao reservationdao = ReservationDao.getInstance(false);
    ValidatorService validate = ValidatorService.getInstance();

    public long updateDebut(String val, int id) throws ServiceException, ParseException {
        try {
            Reservation resa = reservationdao.findResaById(id);

            System.out.println(resa);
            Date fin = resa.getFin();

            int vehicleid = resa.getVehicle_id();
            System.out.print("vehicule id " + vehicleid);
            java.util.Date datenewdebut = new SimpleDateFormat("yyyy-MM-dd").parse(val);

            validate.checkperiodes(datenewdebut, fin, vehicleid, id);
            validate.checkperiodelength(datenewdebut, fin);

            validate.CheckLongResaPeriod(datenewdebut, fin, vehicleid, id, false);

            return reservationdao.updateDebut(val, id);

        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public Reservation findResaById(int idresa) throws ServiceException {
        try {
            return reservationdao.findResaById(idresa);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public long updateFin(String val, int id) throws ServiceException, ParseException {
        try {
            System.out.println("resa id" + id);
            Reservation resa = reservationdao.findResaById(id);
            System.out.println("resa " + resa);
            Date debut = resa.getDebut();
            System.out.println("debut " + debut);
            int vehicleid = resa.getVehicle_id();
            System.out.println("vehicleid " + vehicleid);
            System.out.println("val " + val);
            java.util.Date datenewfin = new SimpleDateFormat("yyyy-MM-dd").parse(val);

            validate.checkperiodes(debut, datenewfin, vehicleid, id);
            validate.CheckLongResaPeriod(debut, datenewfin, vehicleid, id, false);

            validate.checkperiodelength(debut, datenewfin);

            return reservationdao.updateFin(val, id);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public long updateClient(int val, int id) throws ServiceException {
        try {
            return reservationdao.updateClient(val, id);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public long updateVehicle(int val, int id) throws ServiceException {
        try {
            return reservationdao.updateVehicle(val, id);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public List<Reservation> FindAll() throws ServiceException {
        try {
            return reservationdao.findAll();

        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public long create(Reservation reservation) throws ServiceException {
        try {

            Date debut = reservation.getDebut();
            Date fin = reservation.getFin();
            int idresa = reservation.getId();
            int vehicleid = reservation.getVehicle_id();
            System.out.println(debut);
            System.out.println(fin);
            System.out.println("check periodes lengh");
            validate.checkperiodelength(debut, fin);
            System.out.println("check periodes");
            System.out.println(vehicleid);
            validate.checkperiodes(debut, fin, vehicleid, 0);
            validate.CheckLongResaPeriod(debut, fin, vehicleid, idresa, true);

            return reservationdao.create(reservation);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }

    }

    public long deleteReservation(int id) throws ServiceException {
        try {
            return reservationdao.delete(id);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public List<Reservation> findClientReservations(int idclient) throws ServiceException {
        try {
            return reservationdao.findResaByClientId(idclient);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public List<Reservation> findResaByVehicleId(int idvehicle) throws ServiceException {
        try {
            return reservationdao.findResaByVehicleId(idvehicle);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public List<Integer> findVehicleByClientId(int vehicleId) throws ServiceException {
        try {
            return reservationdao.findVehicleByClientId(vehicleId);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public List<Integer> findClientByVehicleId(int idclient) throws ServiceException {
        try {
            return reservationdao.findVehicleByClientId(idclient);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public int findVehicleIdByRentId(int id) throws ServiceException {
        try {
            return reservationdao.findVehicleIdByRentId(id);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public int findClientIdByRentId(int id) throws ServiceException {
        try {
            return reservationdao.findVehicleIdByRentId(id);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public List<Reservation> findResaByClientId(int id) throws ServiceException {
        try {
            return reservationdao.findResaByClientId(id);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public void printReservationChoices() {
        System.out.println("1 - créer une réservation");
        System.out.println("2 - supprimer une réservation");
        System.out.println("3 - réservations relatives à un client");
        System.out.println("4 - reservations relatives à un véhicule");
        System.out.println("5 - toutes les réservations");
        System.out.println("6 - Trouver un vehicule correspondant à une réservation ");
        System.out.println("7 - Trouver un client correspondant à une réservation ");
        System.out.println("8 - Changer le client d' une réservation ");
        System.out.println("9 - Changer le vehicule d' une réservation ");
        System.out.println("10 - Changer la date de debut d' une réservation ");
        System.out.println("11 - Changer la date de fin d' une réservation ");
        System.out.println("12 - menu principal");
    }
}
